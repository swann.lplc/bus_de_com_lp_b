#include <msp430.h> 

int counting = 0;
unsigned int up = 0;
unsigned int cmd = 0;
/**
 * main.c
 */
int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;

    if (CALBC1_1MHZ == 0xFF || CALDCO_1MHZ == 0xFF)
        __bis_SR_register(LPM4_bits);
    else
    {
        DCOCTL = 0;
        BCSCTL1 = CALBC1_1MHZ;
        DCOCTL = CALDCO_1MHZ;
    }

    P1SEL |= BIT6;
    P1SEL2 &= ~BIT6;
    P1DIR |= BIT6;

    P1DIR |= BIT0;

    TA0CTL &= ~MC_0;
    TA0CCR0 = 20000;
    TA0CTL = (TASSEL_2 | MC_1 | ID_0 | TACLR | TAIE);
    TA0CCTL1 = 0 | OUTMOD_7;

    TA0CCR1 = 1550;
    __bis_SR_register(LPM0_bits | GIE);

    return 0;
}
#pragma vector=TIMER0_A1_VECTOR
__interrupt void ma_fnc_timer(void)
{
    if (counting > 2)
    {
        if (!up)
        {
            if (TA0CCR1 > (600 + 95))
            {
                TA0CCR1 -= 95;
            }
            else
            {
                TA0CCR1 = 600;
                up = 1;
            }
        }
        else
        {
            if (TA0CCR1 < (2500 - 95))
            {
                TA0CCR1 += 95;
            }
            else
            {
                TA0CCR1 = 2500;
                up = 0;
            }
        }
        counting = 0;
    }
    else
        counting++;
    TA0CTL &= ~TAIFG;
}
