#include <msp430.h>
#include <string.h>
#include "ADC.h"

#define vitesseMax 200
#define vitesseRot 120

#define PROMPT  "\r\nmaster>"

unsigned char cmd[20];

int virage = 0;                 //0: pas de virage, 1: virage à gauche, 2: virage à droite
int distance = 0;               //distance de l'objet capté(mesure capteur)
int vitesse = 0;                //vitesse linéaire(sans prise en compte des virages)
int avancerReculer = 0;         //1: avancer, 2: reculer
unsigned char P2OUTbis = 0x0;   //rotation des roues(sans prise en compte des virages)

 void init_timer(void){          // init pwm moteur
        WDTCTL = WDTPW + WDTHOLD;

        if( (CALBC1_1MHZ==0xFF) || (CALDCO_1MHZ==0xFF) )
        {
            __bis_SR_register(LPM4_bits);
        }

        P2DIR |= BIT1 | BIT2 | BIT4 | BIT5;
        P2DIR |= ~(BIT0 | BIT3);

        BCSCTL1 = CALBC1_1MHZ;      // choix d'horloge à 1Mhz
        DCOCTL = CALDCO_1MHZ;

        P2DIR |= (BIT2 | BIT4);     //sortie des timers
        P2SEL |= (BIT2 | BIT4);
        P2SEL2 &= ~(BIT2 | BIT4);
        TA1CTL = TASSEL_2 | MC_1;   // mode up

        TA1CCTL1 |= OUTMOD_7;       // activation mode de sortie n°7
        TA1CCTL2 |= OUTMOD_7;

        TA1CCR0 = 1000;             //timer 1ms, 1kHz
        TA1CCR1 = 0;                //roue gauche
        TA1CCR2 = 0;                //roue droite
    }

    void interpreteur(char c)
    {
        if(c == 'h')          //----------------------------------- help
        {
            envoi_msg_UART("\r\nCommandes :");
            envoi_msg_UART("\r\n'z' : Avancer                       |");
            envoi_msg_UART("\r\n's' : Reculer                       |appuyer une 2eme fois sur une commande");
            envoi_msg_UART("\r\n'q' : Tourner à gauche              |annule cell-ci");
            envoi_msg_UART("\r\n'd' : Tourner à droite              |");
            envoi_msg_UART("\r\n\n'ESPACE' : Arreter le robot");
            envoi_msg_UART("\r\n\n'h' : Affiche les commandes\n\r\n");
        }
        else if (c == 'z')
        {
            envoi_msg_UART("\r\n");
            envoi_msg_UART((unsigned char *)cmd);
            envoi_msg_UART("avancer");
            envoi_msg_UART("->");
            envoi_msg_UART("\r\n");
        }
        else if (c == 's')
        {
            envoi_msg_UART("\r\n");
            envoi_msg_UART((unsigned char *)cmd);
            envoi_msg_UART("Reculer\n");
            envoi_msg_UART("->");
            envoi_msg_UART("\r\n");
        }
        else if (c == 'q')
        {
            envoi_msg_UART("\r\n");
            envoi_msg_UART((unsigned char *)cmd);
            envoi_msg_UART("Tourner a gauche\n");
            envoi_msg_UART("->");
            envoi_msg_UART("\r\n");
        }
        else if (c == 'd')
        {
            envoi_msg_UART("\r\n");
            envoi_msg_UART((unsigned char *)cmd);
            envoi_msg_UART("Tourner a droite\n");
            envoi_msg_UART("->");
            envoi_msg_UART("\r\n");
        }
        else if (c == 'a')
        {
            envoi_msg_UART("\r\n");
            envoi_msg_UART((unsigned char *)cmd);
            envoi_msg_UART("Arret\n");
            envoi_msg_UART("->");
            envoi_msg_UART("\r\n");
        }
        else                          //---------------------------- default choice
        {
            envoi_msg_UART("\r\n ?");
            envoi_msg_UART((unsigned char *)cmd);
        }
        envoi_msg_UART(PROMPT);        //---------------------------- command prompt
    }

void InitUART(void)
{
    P1SEL |= (BIT1 | BIT2);                     // P1.1 = RXD, P1.2=TXD
    P1SEL2 |= (BIT1 | BIT2);                    // P1.1 = RXD, P1.2=TXD
    UCA0CTL1 = UCSWRST;                         // SOFTWARE RESET
    UCA0CTL1 |= UCSSEL_3;                       // SMCLK (2 - 3)

    P1DIR |= BIT0;
    P1OUT &= ~BIT0;

    UCA0CTL0 &= ~(UCPEN | UCMSB | UCDORM);
    UCA0CTL0 &= ~(UC7BIT | UCSPB | UCMODE_3 | UCSYNC); // dta:8 stop:1 usci_mode3uartmode
    UCA0CTL1 &= ~UCSWRST;                   // **Initialize USCI state machine**

    UCA0BR0 = 104;                              // 1MHz, OSC16, 9600 (8Mhz : 52) : 8/115k
    UCA0BR1 = 0;                                // 1MHz, OSC16, 9600
    UCA0MCTL = 10;

    /* Enable USCI_A0 RX interrupt */
    IE2 |= UCA0RXIE;
}

void TXdata( unsigned char c )
{
    while (!(IFG2 & UCA0TXIFG));  // USCI_A0 TX buffer ready?
    UCA0TXBUF = c;              // TX -> RXed character
}

    void envoi_msg_UART(unsigned char *msg)
    {
        unsigned int i = 0;
        for(i=0 ; msg[i] != 0x00 ; i++)
        {
            while(!(IFG2 & UCA0TXIFG));    //attente de fin du dernier envoi (UCA0TXIFG à 1 quand UCA0TXBUF vide)
            UCA0TXBUF=msg[i];
        }
    }

    void sensRotRoues(int sens){    //set du sens de rotation des roues
                                    //sans prendre en compte les virages
        if (sens == 1){
            P2OUTbis &= ~BIT1;     // roue gauche avance
            P2OUTbis |= BIT5;      // roue droite avance
            avancerReculer = 1;    //le robot avance
        }else {
            P2OUTbis |= BIT1;      // roue gauche recule
            P2OUTbis &= ~BIT5;     // roue droite recule
            avancerReculer = 2;    //le robot recul
        }
    }

    int infra(){                       // getter valeur infrarouge
        ADC_Demarrer_conversion(0);
        int G = ADC_Lire_resultat();
        return G;
    }

    void vaccumDetect(){
        distance = infra();
        if(distance < 600){
            TA1CCR1 = 0;
            TA1CCR2 = 0;
        }
        else{
        }
    }

    void gestionVirages(){
        switch(virage){
            case 0 :                //pas de virage
                TA1CCR1 = vitesse;                      //roue gauche
                TA1CCR2 = vitesse;                      //roue droite
                P2OUT = P2OUTbis;                   //conservation des sens de rotation
                break;
            case 1 :                //virage à gauche
                if(vitesse > vitesseRot){             //si rotation possible sans changement de sens
                    P2OUT = P2OUTbis;               //conservation des sens de rotation
                    if(avancerReculer == 1){            //si le robot avance
                        TA1CCR2 = vitesse + vitesseRot;            //accélération de la roue droite
                        TA1CCR1 = vitesse - vitesseRot;            //ralentissement de la roue gauche
                    } else{                             //si le robot recule
                        TA1CCR2 = vitesse - vitesseRot;            //ralentissement de la roue droite
                        TA1CCR1 = vitesse + vitesseRot;            //accélération de la roue gauche
                    }

                }
                else{                           //sinon
                    if(avancerReculer == 1){        //si le robot avance
                        P2OUT = P2OUTbis ^ BIT1;        //changement de sens de la roue gauche
                        TA1CCR1 = vitesseRot - vitesse;            //roue gauche
                        TA1CCR2 = vitesseRot + vitesse;            //accélération de la roue droite
                    }else{
                        P2OUT = P2OUTbis ^ BIT5;        //changement de sens de la roue droite
                        TA1CCR2 = vitesseRot - vitesse;            //ralentissement de la roue droite
                        TA1CCR1 = vitesseRot + vitesse;            //accélération de la roue gauche
                    }
                }
                break;
            case 2 :                //virage à droite
                if(vitesse > vitesseRot){              //si rotation possible sans changement de sens
                    P2OUT = P2OUTbis;               //conservation des sens de rotation
                    if(avancerReculer == 1){            //si le robot avance
                        TA1CCR2 = vitesse - vitesseRot;            //ralentissement de la roue droite
                        TA1CCR1 = vitesse + vitesseRot;            //accélération de la roue gauche
                    }else{                              //si le robot recule
                        TA1CCR2 = vitesse + vitesseRot;            //accélération de la roue droite
                        TA1CCR1 = vitesse - vitesseRot;            //ralentissement de la roue gauche
                    }
                }
                else{                           //sinon
                    if(avancerReculer == 1){        //si le robot avance
                        P2OUT = P2OUTbis ^ BIT5;        //changement de sens de la roue droite
                        TA1CCR2 = vitesseRot - vitesse;                //ralentissement de la roue droite
                        TA1CCR1 = vitesseRot + vitesse;                //accélération de la roue gauche
                    }else{
                        P2OUT = P2OUTbis ^ BIT1;        //changement de sens de la roue gauche
                        TA1CCR1 = vitesseRot - vitesse;            //roue gauche
                        TA1CCR2 = vitesseRot + vitesse;            //accélération de la roue droite
                    }
                }
                break;
            default :
                break;
        }

    }

void main(void)
{
    // Stop watchdog timer to prevent time out reset
    WDTCTL = WDTPW | WDTHOLD;

    if(CALBC1_1MHZ==0xFF || CALDCO_1MHZ==0xFF)
    {
        __bis_SR_register(LPM4_bits); // Low Power Mode #trap on Error
    }
    else
    {
        // Factory parameters
        BCSCTL1 = CALBC1_1MHZ;
        DCOCTL = CALDCO_1MHZ;
    }
    init_timer();
    ADC_init();
    InitUART();

    __bis_SR_register(GIE); // interrupts enabled

    TXdata('>');
    while(1){
          vaccumDetect();
    }
}

// Echo back RXed character, confirm TX buffer is ready first
#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    unsigned char c;

    c = UCA0RXBUF;
    interpreteur(c);
    if(c == 'z'){
        if(vitesse == 0){
            vitesse = vitesseMax;
            sensRotRoues(1);
        }
        else{
            vitesse = 0;
        }
    }
    else if(c == 's'){
        if(vitesse == 0){
            vitesse = vitesseMax;
            sensRotRoues(2);
        }
        else{
            vitesse = 0;
        }
    }
    else if(c == 'q'){
        if(virage == 0){
            virage = 1;
        }
        else{
            virage = 0;
        }
    }
    else if(c == 'd'){
        if(virage == 0){
            virage = 2;
        }
        else{
            virage = 0;
        }
    }
    else if(c == 'a'){
        sensRotRoues(0);
        virage = 0;
        vitesse = 0;
    }
    gestionVirages(vitesse);
    TXdata(c);
    TXdata(c);
}
